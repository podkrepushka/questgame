﻿using System.Web;
using System.Web.Optimization;

namespace QuestGame
{
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/ajax").Include(
                        "~/scripts/jquery.unobtrusive-ajax.js"));

            bundles.Add(new ScriptBundle("~/bundles/submenu").Include(
                        "~/scripts/bootstrap-submenu.js",
                        "~/scripts/docs.js"));

            bundles.Add(new ScriptBundle("~/bundles/highcharts").Include(
                        "~/Scripts/Highcharts-4.0.1/js/highcharts.js",
                        "~/Scripts/Highcharts-4.0.1/js/modules/exporting.js"));

            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
                        "~/Scripts/moment-with-locales.js",
                        "~/Scripts/datepicker-locale-ru.js",
                        "~/Scripts/bootstrap-datetimepicker.js"));
  
            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/chosen").Include(
                      "~/Scripts/chosen.jquery.js"));///////

            bundles.Add(new StyleBundle("~/Content/chosen").Include(
                        "~/Content/chosen.bootstrap.css"));///////

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-theme.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/Admin").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-theme.css",
                      "~/Content/AdminPanel.css"));

            bundles.Add(new StyleBundle("~/Content/submenu").Include(
                    "~/Content/bootstrap-submenu.css"));

            bundles.Add(new StyleBundle("~/Content/datepicker").Include(
                    "~/Content/bootstrap-datetimepicker.css"));

        }
    }
}
