﻿using System.Web.Mvc;

using QuestGame.Areas.Admin.Models;
using QuestGame.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Model;
using System.IO;



namespace QuestGame.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class MainController : Controller
    {
        // GET: Admin/Main
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}