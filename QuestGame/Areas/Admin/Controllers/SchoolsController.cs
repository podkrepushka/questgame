﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuestGame.Models;
using QuestGame.Areas.Admin.Models;
using System.Collections;

namespace QuestGame.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SchoolsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/Schools
        public async Task<ActionResult> Index()
        {
            return View(await db.Schools.ToListAsync());
        }


        // GET: Admin/Schools/Create
        public ActionResult Create()
        {
            ViewBag.Games = new SelectList(db.Games, "ID", "Name");
            return View();
        }

        // POST: Admin/Schools/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(SchoolViewModel model)
        {
            if (ModelState.IsValid)
            {
                var school = new School
                {
                    Name = model.Name
                };
                if(model.GamesID != null)
                {
                    foreach (var game in model.GamesID)
                    {
                        school.Games.Add(await db.Games.FindAsync(game));
                    }
                }
                db.Schools.Add(school);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.Games = new SelectList(db.Games, "ID", "Name");
            return View(model);
        }

        // GET: Admin/Schools/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            School school = await db.Schools.FindAsync(id);

            if (school == null)
            {
                return HttpNotFound();
            }

            var model = new SchoolViewModel
            {
                ID = school.ID,
                Name = school.Name,
                GamesID = school.Games.Select(g => g.ID).ToArray()
            };
            ViewBag.Games = new SelectList(db.Games, "ID", "Name");
            return View(model);
        }

        // POST: Admin/Schools/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name,GamesID")] SchoolViewModel model)
        {
            if (ModelState.IsValid)
            {
                var school = await db.Schools.FindAsync(model.ID);

                if (school == null)
                {
                    return HttpNotFound();
                }
                school.Name = model.Name;
                school.Update();
                school.Games.Clear();

                if (model.GamesID != null)
                {
                    var games = db.Games.Where(g => model.GamesID.Contains(g.ID));
                    
                    foreach (var game in games)
                    {
                        school.Games.Add(game);
                    }
                }
                    
                db.Entry(school).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Admin/Schools/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            School school = await db.Schools.FindAsync(id);
            if (school == null)
            {
                return HttpNotFound();
            }
            return View(school);

        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            School school = await db.Schools.FindAsync(id);
            db.Schools.Remove(school);
            try
            {
                await db.SaveChangesAsync();
                db.SchoolsDelete.Add(new SchoolDelete() { ID = school.ID });
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            catch
            {
                ModelState.AddModelError("", "Данное учебное заведение ссылается на пользователей!");
            }
            return View(school);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
