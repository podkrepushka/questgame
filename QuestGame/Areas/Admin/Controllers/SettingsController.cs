﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QuestGame.Areas.Admin.Models;
using QuestGame.Models;
using System.Threading.Tasks;
namespace QuestGame.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SettingsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult SyncService()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> SyncService(SettingsSyncServiceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var tempGames = db.GamesDelete.Where(d => d.DateDelete < model.Date);
                var tempStages = db.StagesDelete.Where(d => d.DateDelete < model.Date);
                var tempSchools = db.SchoolsDelete.Where(d => d.DateDelete < model.Date);
                var tempLocations = db.LocationsDelete.Where(d => d.DateDelete < model.Date);
                var tempSubjects = db.SubjectsDelete.Where(d => d.DateDelete < model.Date);
                db.GamesDelete.RemoveRange(tempGames);
                db.StagesDelete.RemoveRange(tempStages);
                db.SchoolsDelete.RemoveRange(tempSchools);
                db.LocationsDelete.RemoveRange(tempLocations);
                db.SubjectsDelete.RemoveRange(tempSubjects);
                
                try
                {
                    await db.SaveChangesAsync();
                    return View("ClearDataSyncServiceOk");
                    
                }
                catch
                {
                    ModelState.AddModelError("", "Ошибка при удалении данных");
                }
            }
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}