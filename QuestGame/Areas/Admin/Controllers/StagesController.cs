﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuestGame.Models;

namespace QuestGame.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class StagesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/Stages/Create
        public async Task<ActionResult> Create(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var game = await db.Games.FindAsync(id.Value);
            ViewBag.Games = new SelectList(db.Games, "ID", "Name",id.Value);
            ViewBag.Location = new SelectList(db.Locations, "ID", "Name");

            ViewBag.GameID = id.Value;
            return View(new Stage { Position = game.Stages.Count +1});
        }

        // POST: Admin/Stages/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "GameID,Question,Answer,QRQuestion,Help,LocationID,Position")] Stage stage)
        {
            if (ModelState.IsValid)
            {
                db.Stages.Add(stage);
                await db.SaveChangesAsync();
                return RedirectToAction("Details","Games", new { id = stage.GameID });
            }
            ViewBag.Games = new SelectList(db.Games, "ID", "Name", stage.GameID);
            ViewBag.Location = new SelectList(db.Locations, "ID", "Name",stage.LocationID);
            ViewBag.GameID = stage.GameID;
            return View(stage);
        }

        // GET: Admin/Stages/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stage stage = await db.Stages.FindAsync(id);
            if (stage == null)
            {
                return HttpNotFound();
            }
            ViewBag.Games = new SelectList(db.Games, "ID", "Name", stage.GameID);
            ViewBag.Location = new SelectList(db.Locations, "ID", "Name", stage.LocationID);

            return View(stage);
        }

        // POST: Admin/Stages/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,GameID,Question,Answer,QRQuestion,Help,LocationID,Position,DateSync")] Stage stage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(stage).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Details", "Games", new { id = stage.GameID });
            }
            ViewBag.Games = new SelectList(db.Games, "ID", "Name", stage.GameID);
            ViewBag.Location = new SelectList(db.Locations, "ID", "Name", stage.LocationID);

            return View(stage);
        }

        // GET: Admin/Stages/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stage stage = await db.Stages.FindAsync(id);
            if (stage == null)
            {
                return HttpNotFound();
            }
            return View(stage);
        }

        // POST: Admin/Stages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Stage stage = await db.Stages.FindAsync(id);
            db.Stages.Remove(stage);
            db.StagesDelete.Add(new StageDelete() { ID = stage.ID });
            await db.SaveChangesAsync();
            return RedirectToAction("Details", "Games", new { id = stage.GameID });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
