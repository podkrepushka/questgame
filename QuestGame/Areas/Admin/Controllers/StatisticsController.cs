﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuestGame.Models;

using QuestGame.Areas.Admin.Models.Statistics;

using QuestGame.Extensions;
using QuestGame.Filters;

using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using QuestGame.Helpers;

namespace QuestGame.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class StatisticsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        [HttpGet]
        public async Task<ActionResult> Answers()
        {
            return View(await db.AnswersUser.OrderByDescending(ans => ans.DateAnswer).ToListAsync());
        }

        // GET: Admin/Statistics/ActiveUsers
        [HttpGet]
        public ActionResult ActiveUsers()
        {
            var model = new ActiveOrPercentViewModel();
            model.Games = new SelectList(db.Games, "ID", "Name");
            model.Schools = new SelectList(db.Schools, "ID", "Name");

            return View(model);
        }

        // POST: Admin/Statistics/ActiveUsers отчет в виде графика
        [HttpPost]
        [HttpParamAction]
        public async Task<ActionResult> ActiveUsersGraph(ActiveOrPercentViewModel model)
        {

            model.Games = new SelectList(db.Games, "ID", "Name");
            model.Schools = new SelectList(db.Schools, "ID", "Name");
            if (ModelState.IsValid)
            {
                var stats = await GetActiveUsers(model).ToListAsync();
                var game = (await db.Games.FindAsync(model.GameID)).Name;

                var Title = String.Format("Активность участников игры {0} c {1} до {2}", game,
                                          model.StartDate.Value.ToShortDateString(), model.EndDate.Value.ToShortDateString());
                string SubTitle = "";
                if (model.SchoolsID != null)
                {
                    var schools = await db.Schools.Where(s => model.SchoolsID.Contains(s.ID)).Select(s => s.Name).ToArrayAsync();
                    if (schools.Length != 0)
                    {
                        SubTitle = String.Format("по учебным заведениям: {0}", String.Join(", ", schools));
                    }
                }
                else
                {
                    SubTitle = "по всем учебным заведениям";
                }
                    
                var Points = new List<object>();
                foreach (var s in stats)
                {
                    Points.Add(new { X = s.DateAnswer, Y = s.CountAnswers });
                }

                model.Chart = ChartHelper.GetChart(ChartTypes.Area, AxisTypes.Datetime,Points.ToArray(),"Дата активности участников", "Количество ответов",Title, "Активность участников",SubTitle);
            }

            return View(model);
        }


        // POST: Admin/Statistics/ActiveUsers отчет в виде файла excel
        [HttpPost]
        [HttpParamAction]
        public async Task<ActionResult> ActiveUsersExcel(ActiveOrPercentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var active = await GetActiveUsers(model).ToListAsync();
                var game = (await db.Games.FindAsync(model.GameID)).Name;
                var Titles = new List<string> 
                {
                    "Дата активности участников",
                    "Количество ответов"
                };
                
                var ExtraData = new List<string[]>();
                ExtraData.Add(new string[] { "Сценарий игры", game });
                ExtraData.Add(new string[] { "Начало периода", model.StartDate.Value.ToShortDateString() });
                ExtraData.Add(new string[] { "Конец периода", model.EndDate.Value.ToShortDateString() });
                if (model.SchoolsID != null)
                {
                    ExtraData.Add(new string[] { "Учебные заведения", String.Join(", ", db.Schools.Where(s => model.SchoolsID
                                                                                                                   .Contains(s.ID))
                                                                                                                   .Select(s => s.Name)
                                                                                                                   .ToArray()) });

                }
                return File(active.ExportExcel(Titles,ExtraData), "application/vnd.ms-excel", String.Format("ActiveUsersOnDays-{0}.xls", DateTime.Now.ToString()));
            }
            model.Games = new SelectList(db.Games, "ID", "Name");
            model.Schools = new SelectList(db.Schools, "ID", "Name");
            return View(model);
        }
        private IQueryable<ActiveUsersDate> GetActiveUsers(ActiveOrPercentViewModel model)
        {
            IQueryable<AnswerUser> answers = db.AnswersUser;
            if (model.SchoolsID != null)
            {
                answers = answers.Where(ans => model.SchoolsID.Contains(ans.User.SchoolID.Value));
            }

            var active = answers.Where(a => a.DateAnswer >= model.StartDate & a.DateAnswer <= model.EndDate)
                                .Where(s => s.Stage.GameID.Equals(model.GameID))
                                .GroupBy(d => d.DateAnswer)
                                .Select(ans => new ActiveUsersDate { DateAnswer = ans.Key, CountAnswers = ans.Count() })
                                .OrderBy(sort => sort.DateAnswer);
            return active;
        }

        // GET: Admin/Statistics/CompleteGame
        [HttpGet]
        public ActionResult CompleteGame()
        {
            var model = new CompleteGameViewModel();
            //model.Date = DateTime.Now;
            model.Games = new SelectList(db.Games.Where(g => g.Stages.Count > 0), "ID", "Name");
            model.Schools = new SelectList(db.Schools, "ID", "Name");
            return View(model);
        }

        // POST: Admin/Statistics/CompleteGame Отчет в виде графика
        [HttpPost]
        [HttpParamAction]
        public async Task<ActionResult> CompleteGameGraph(CompleteGameViewModel model)
        {
            if (ModelState.IsValid)
            {
                var percentGame = await GetPercentCompleteGame(model);
                if (percentGame != null)
                {
                    var game = (await db.Games.FindAsync(model.GameID)).Name;
                    var Title = String.Format("Процент завершения игры {0} на {1}",game, model.Date.Value.ToShortDateString());
                    string SubTitle = "";
                    if (model.SchoolsID != null)
                    {
                        var schools = await db.Schools.Where(s => model.SchoolsID.Contains(s.ID)).Select(s => s.Name).ToArrayAsync();
                        if (schools.Length != 0)
                        {
                            SubTitle = String.Format("по учебным заведениям: {0}", String.Join(", ", schools));
                        }
                    }
                    else
                    {
                        SubTitle = "по всем учебным заведениям";
                    }
                    model.Chart = ChartHelper.GetChart(ChartTypes.Column, AxisTypes.Linear, percentGame.HelpGraphGetValueXY(), "Процент завершения игры",
                                                     "Количество участников игры", Title, "Процент завершения игры",SubTitle);
                }
                else
                    ModelState.AddModelError("", "У данной игры нет этапов");
            }
            model.Games = new SelectList(db.Games.Where(g => g.Stages.Count > 0), "ID", "Name");
            model.Schools = new SelectList(db.Schools, "ID", "Name");
            return View(model);
        }

        // POST: Admin/Statistics/CompleteGame отчет в виде файла excel
        [HttpPost]
        [HttpParamAction]
        public async Task<ActionResult> CompleteGameExcel(CompleteGameViewModel model)
        {
            if (ModelState.IsValid)
            {
                var percentGame = await GetPercentCompleteGame(model);
                if (percentGame != null)
                {
                    var Titles = new List<string> 
                    {
                        "Процент заверешния игры",
                        "Количество участников"
                    };
                    var game = (await db.Games.FindAsync(model.GameID)).Name;
                    var ExtraData = new List<string[]>();
                    ExtraData.Add(new string[] { "Сценарий игры", game });
                    ExtraData.Add(new string[] { "Дата прохождения игры",model.Date.Value.ToShortDateString()});
                    if (model.SchoolsID != null)
                    {
                        ExtraData.Add(new string[] { "Учебные заведения", String.Join(", ", db.Schools.Where(s => model.SchoolsID
                                                                                                                   .Contains(s.ID))
                                                                                                                   .Select(s => s.Name)
                                                                                                                   .ToArray()) });

                    }
                    return File(percentGame.GetStatsCompleteGame().ExportExcel(Titles,ExtraData), "application/vnd.ms-excel", String.Format("PercentCompleteGameOnDays-{0}.xls", DateTime.Now.ToString()));
                }
                ModelState.AddModelError("", "У данной игры нет этапов");
                return View();
            }
            model.Games = new SelectList(db.Games.Where(g => g.Stages.Count > 0), "ID", "Name");
            model.Schools = new SelectList(db.Schools, "ID", "Name");
            return View(model);
        }

        private async Task<CompleteGameDate> GetPercentCompleteGame(CompleteGameViewModel model)
        {
            var percentGame = new CompleteGameDate();
            var game = await db.Games.FindAsync(model.GameID);
            if (game.Stages.Count.Equals(0))
            {
                return null;
            }
            var AnswersUsers = db.AnswersUser.Where(a => a.DateAnswer == model.Date & a.Stage.GameID.Equals(model.GameID));
            if (model.SchoolsID != null)
            {
                AnswersUsers = AnswersUsers.Where(ans => model.SchoolsID.Contains(ans.User.SchoolID.Value));
            }
            var AnswersUsersGroup = AnswersUsers.GroupBy(a => a.UserID);
            foreach (var answeruser in AnswersUsersGroup)
            {
                var countcompletestages = answeruser.Where(a => a.AnswerCorrect == true)
                                                    .GroupBy(a => a.StageID)
                                                    .Count();

                percentGame.AddPercentCompleteGame(((double)countcompletestages / (double)game.Stages.Count) * 100);
            }
            return percentGame;
        }

        // GET: Admin/Statistics/AverageCompleteGame
        [HttpGet]
        public ActionResult AverageCompleteGame()
        {
            var model = new ActiveOrPercentViewModel();
            model.Games = new SelectList(db.Games.Where(g => g.Stages.Count > 0), "ID", "Name");
            model.Schools = new SelectList(db.Schools, "ID", "Name");
            return View(model);
        }

        // POST: Admin/Statistics/AverageCompleteGame Отчет в виде графика
        [HttpPost]
        [HttpParamAction]
        public async Task<ActionResult> AverageCompleteGameGraph(ActiveOrPercentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var ListACG = await GetAveragePercentCompleteGame(model);
                var game = (await db.Games.FindAsync(model.GameID)).Name;
                var Title = String.Format("Средний процент завершения игры {0} c {1} по {2}",game, 
                                          model.StartDate.Value.ToShortDateString(), model.EndDate.Value.ToShortDateString());
                string SubTitle = "";
                if (model.SchoolsID != null)
                {
                    var schools = await db.Schools.Where(s => model.SchoolsID.Contains(s.ID)).Select(s => s.Name).ToArrayAsync();
                    if (schools.Length != 0)
                    {
                        SubTitle = String.Format("по учебным заведениям: {0}", String.Join(", ", schools));
                    }
                }
                else
                {
                    SubTitle = "по всем учебным заведениям";
                }
                var Points = new List<object>();
                foreach (var a in ListACG)
                {
                    Points.Add(new { X = a.DateAnswer, Y = a.PercentCompleteGame});
                }

                model.Chart = Helpers.ChartHelper.GetChart(ChartTypes.Area, AxisTypes.Datetime, Points.ToArray(), "Дата активности участников",
                                                         "Средний процент завершения игры", Title, "Средний процент завершения игры",SubTitle);

            }
            model.Games = new SelectList(db.Games.Where(g => g.Stages.Count > 0), "ID", "Name");
            model.Schools = new SelectList(db.Schools, "ID", "Name");
            return View(model);
        }

        // POST: Admin/Statistics/AverageCompleteGame отчет в виде файла excel
        [HttpPost]
        [HttpParamAction]
        public async Task<ActionResult> AverageCompleteGameExcel(ActiveOrPercentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var ListACG = await GetAveragePercentCompleteGame(model);
                var game = (await db.Games.FindAsync(model.GameID)).Name;
                var Titles = new List<string> 
                {
                        "Дата ответа на вопрос",
                        "Процент завершения"
                };
                var ExtraData = new List<string[]>();
                ExtraData.Add(new string[] { "Сценарий игры", game });
                ExtraData.Add(new string[] { "Начало периода", model.StartDate.Value.ToShortDateString() });
                ExtraData.Add(new string[] { "Конец периода", model.EndDate.Value.ToShortDateString() });
                if (model.SchoolsID != null)
                {
                    ExtraData.Add(new string[] { "Учебные заведения", String.Join(", ", db.Schools.Where(s => model.SchoolsID
                                                                                                                   .Contains(s.ID))
                                                                                                                   .Select(s => s.Name)
                                                                                                                   .ToArray()) });

                }

                return File(ListACG.ExportExcel(Titles,ExtraData), "application/vnd.ms-excel", String.Format("AveragePercentCompleteGameOnDays-{0}.xls", DateTime.Now.ToString()));
            }
            model.Games = new SelectList(db.Games.Where(g => g.Stages.Count > 0), "ID", "Name");
            model.Schools = new SelectList(db.Schools, "ID", "Name");
            return View(model);
        }
        private async Task<List<AverageCompleteGameDate>> GetAveragePercentCompleteGame(ActiveOrPercentViewModel model)
        {
            var game = await db.Games.FindAsync(model.GameID);
            if (game.Stages.Count.Equals(0))
            {
                return null;
            }
            IQueryable<AnswerUser> answer = db.AnswersUser;
            if (model.SchoolsID != null)
            {
                answer = answer.Where(ans => model.SchoolsID.Contains(ans.User.SchoolID.Value));
            }
            var GroupDateAnswer = answer.Where(a => a.Stage.GameID.Equals(model.GameID))
                                        .Where(ans => ans.DateAnswer >= model.StartDate & ans.DateAnswer <= model.EndDate)
                                        .GroupBy(a => a.DateAnswer);
            var ListACG = new List<AverageCompleteGameDate>();
            foreach (var answersusers in GroupDateAnswer)
            {
                var GroupUsers = answersusers.GroupBy(a => a.UserID);
                int CountUsers = GroupUsers.Count();
                int TempCountUsers = 0;
                foreach (var ansuser in GroupUsers)
                {
                    var completestages = ansuser.Where(a => a.AnswerCorrect == true)
                                                .GroupBy(a => a.StageID)
                                                .Count();
                    if (completestages == game.Stages.Count)
                    {
                        TempCountUsers++;
                    }
                }
                ListACG.Add(new AverageCompleteGameDate { DateAnswer = answersusers.Key, PercentCompleteGame = TempCountUsers * 100 /CountUsers });     
            }
            return ListACG;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}