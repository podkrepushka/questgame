﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuestGame.Models;
using QuestGame.Areas.Admin.Models.Users;
using Microsoft.AspNet.Identity.Owin;

using QuestGame.Extensions;
using QuestGame.Filters;
using AutoMapper;

namespace QuestGame.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private ApplicationUserManager _userManager;

        private ApplicationDbContext db = new ApplicationDbContext();

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Admin/Users
        [HttpGet]
        public ActionResult Index()
        {
            Mapper.CreateMap<ApplicationUser, IndexUserViewModel>()
                  .ForMember(iu => iu.School, opt => opt.MapFrom(u => u.School.Name));
            var model = new ListUsersViewModel();
            model.Users = Mapper.Map<IQueryable<ApplicationUser>, List<IndexUserViewModel>>(db.Users);
            model.Schools = new SelectList(db.Schools, "ID", "Name");
            model.Subjects = new SelectList(db.Subjects, "ID", "Name");
            return View(model);
        }

        // POST: Admim/Users/Index
        [HttpPost]
        [HttpParamAction]
        public ActionResult WebReport(List<int> SchoolsID, List<int> SubjectsID)
        {
            var model = new ListUsersViewModel();
            Mapper.CreateMap<ApplicationUser, IndexUserViewModel>()
                  .ForMember(iu => iu.School, opt => opt.MapFrom(u => u.School.Name));
            model.Users = Mapper.Map<IQueryable<ApplicationUser>, List<IndexUserViewModel>>(GetUsersUseFilter(SchoolsID, SubjectsID));
            model.Schools = new SelectList(db.Schools, "ID", "Name");
            model.Subjects = new SelectList(db.Subjects, "ID", "Name");

            return View(model);
        }

        // POST: Admim/Users/Index
        [HttpParamAction]
        public async Task<ActionResult> ExcelReport(List<int> SchoolsID, List<int> SubjectsID)
        {
            var UsersList = await GetUsersUseFilter(SchoolsID, SubjectsID).ToListAsync();
            var Users = UsersList.Select(u => new
                {
                    ID = u.Id,
                    Name = u.Name,
                    Surname = u.Surname,
                    School = u.School != null ? u.School.Name : "",
                    Email = u.Email,
                    EmailConfirmed = u.EmailConfirmed ? "Да" : "Нет",
                    Subjects = String.Join(", ",u.Subjects.Select(s => s.Name)),
                    Date = u.CreateAccount
                });
            var TitlesUser = new List<string> 
            {
                "ID",
                "Имя",
                "Фамилия",
                "Учебное заведение",
                "Адрес электронной почты",
                "Подтверждён адрес электронной почты",
                "Сдаваемые предметы ЕГЭ",
                "Дата регистрации"
            };
            var ExtraData = new List<string[]>();
            if (SchoolsID != null)
            {
                ExtraData.Add(new string[] { "Учебные заведения",String.Join(", ", db.Schools
                                                                                 .Where(s => SchoolsID.Contains(s.ID))
                                                                                 .Select(s => s.Name)
                                                                                 .ToArray()) }); 
            }
            if (SubjectsID != null)
            {
                ExtraData.Add(new string[] { "Предметы сдаваемые на ЕГЭ",String.Join(", ", db.Subjects
                                                                                         .Where(s => SubjectsID.Contains(s.ID))
                                                                                         .Select(s => s.Name)
                                                                                         .ToArray()) });
            }
            
            ExtraData.Add(new string[] { "Дата отчета",DateTime.Now.ToString() });
            
            return File(Users.ExportExcel(TitlesUser, ExtraData), "application/vnd.ms-excel", String.Format("Users-{0}.xls", DateTime.Now.ToString()));
        }

        private IQueryable<ApplicationUser> GetUsersUseFilter(List<int> SchoolsID, List<int> SubjectsID)
        {
            IQueryable<ApplicationUser> users = db.Users;
            if (SchoolsID != null)
            {
                users = users.Where(u => SchoolsID.Contains(u.SchoolID.Value));
            }
            if (SubjectsID != null)
            {
                foreach (var subjectID in SubjectsID)
                {
                    users = users.Where(u => u.Subjects.Contains(db.Subjects.FirstOrDefault(s => s.ID == subjectID)));
                }
            }
            return users;
        }

        // GET: Admin/Users/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = await UserManager.FindByIdAsync(id.Value);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            Mapper.CreateMap<ApplicationUser, DetailsUserViewModel>()
                  .ForMember(du => du.School, opt => opt.MapFrom(u => u.School.Name))
                  .ForMember(du => du.Subjects, opt => opt.MapFrom(u => String.Join(", ", u.Subjects.Select(s => s.Name))));

            var model = Mapper.Map<ApplicationUser, DetailsUserViewModel>(applicationUser);
            model.Role = String.Join(", ", await UserManager.GetRolesAsync(model.Id));

            return View(model);
        }

        // GET: Admin/Users/Create
        public ActionResult Create()
        {
            var model = new UserViewModel();
            model.Schools = new SelectList(db.Schools, "ID", "Name");
            model.Subjects = new SelectList(db.Subjects, "ID", "Name");
            model.Roles = new SelectList(db.Roles.Select(r => r.Name));
            return View(model);
        }

        // POST: Admin/Users/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(UserViewModel model)
        {
            if (ModelState.IsValid & model.Password != null)
            {
                Mapper.CreateMap<UserViewModel,ApplicationUser>()
                      .ForMember(cu => cu.UserName, opt => opt.MapFrom(u => u.Email))
                      .ForMember(cu => cu.CreateAccount, opt => opt.MapFrom(u => DateTime.Now))
                      .ForMember(cu => cu.EmailConfirmed, opt => opt.MapFrom(u => true));

                var user = Mapper.Map<UserViewModel, ApplicationUser>(model);

                var result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    //Добавление предметов ЕГЭ
                    if (model.SubjectsID != null)
                    {
                        var usertmp = await db.Users.FirstAsync(u => u.Id.Equals(user.Id));
                        foreach (var subjectID in model.SubjectsID)
                        {
                            usertmp.Subjects.Add(await db.Subjects.FindAsync(subjectID));
                        }
                        db.Entry(usertmp).State = EntityState.Modified;
                        await db.SaveChangesAsync();                     
                    }
                    //
                    await UserManager.AddToRoleAsync(user.Id, model.Role);
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", String.Format("Пользователь с эл. адресом {0} уже существует", model.Email));
            }
            if (model.Password == null)
            {
                ModelState.AddModelError("Password", "Требуется поле пароль");
            }
            model.Schools = new SelectList(db.Schools, "ID", "Name");
            model.Subjects = new SelectList(db.Subjects, "ID", "Name");
            model.Roles = new SelectList(db.Roles.Select(r => r.Name));
            return View(model);
        }

        // GET: Admin/Users/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = await UserManager.FindByIdAsync(id.Value);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            Mapper.CreateMap<ApplicationUser, UserViewModel>()
                  .ForMember(uv => uv.Subjects, opt => opt.Ignore())
                  .ForMember(uv => uv.Roles, opt => opt.Ignore())
                  .ForMember(uv => uv.Schools, opt => opt.Ignore());

            var model = Mapper.Map<ApplicationUser, UserViewModel>(applicationUser);
            model.Schools = new SelectList(db.Schools, "ID", "Name");
            model.Roles = new SelectList(db.Roles.Select(r => r.Name));
            model.Role = (await UserManager.GetRolesAsync(model.Id)).First();
            return View(model);
        }

        // POST: Admin/Users/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser User = await UserManager.FindByIdAsync(model.Id);
                if (User == null)
                {
                    return HttpNotFound();
                }

                User.UserName = model.Email;
                User.Email = model.Email;
                User.Surname = model.Surname;
                User.Name = model.Name;
                User.SchoolID = model.SchoolID;
                User.EmailConfirmed = true;

                var result = await UserManager.UpdateAsync(User);
                if (result.Succeeded)
                {
                    if (!await UserManager.IsInRoleAsync(User.Id, model.Role))
                    {
                        await UserManager.RemoveFromRoleAsync(User.Id, "Admin");
                        await UserManager.RemoveFromRoleAsync(User.Id, "User");
                        await UserManager.AddToRolesAsync(model.Id, model.Role);
                    }
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", String.Format("Пользователь с эл. адресом {0} уже существует", model.Email));


            }
            model.Schools = new SelectList(db.Schools, "ID", "Name", model.SchoolID);
            model.Roles = new SelectList(db.Roles.Select(r => r.Name));

            return View(model);
        }

        // GET: Admin/Users/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = await UserManager.FindByIdAsync(id.Value);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }

            Mapper.CreateMap<ApplicationUser, DetailsUserViewModel>()
                 .ForMember(du => du.School, opt => opt.MapFrom(u => u.School.Name))
                 .ForMember(du => du.Subjects, opt => opt.MapFrom(u => String.Join(", ", u.Subjects.Select(s => s.Name))));

            var model = Mapper.Map<ApplicationUser, DetailsUserViewModel>(applicationUser);
            model.Role = String.Join(", ", await UserManager.GetRolesAsync(model.Id));

            return View(model);
        }

        // POST: Admin/Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ApplicationUser applicationUser = await UserManager.FindByIdAsync(id);
            if (applicationUser != null)
                await UserManager.DeleteAsync(applicationUser);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }
            }
            base.Dispose(disposing);
        }
    }
}
