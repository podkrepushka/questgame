﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QuestGame.Areas.Admin.Models
{
    public class SchoolViewModel
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "Название учебного заведения")]
        public string Name { get; set; }

        [Display(Name = "Участие в играх")]
        public int[] GamesID { get; set; }
    }
}