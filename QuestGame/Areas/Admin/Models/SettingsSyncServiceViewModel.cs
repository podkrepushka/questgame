﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace QuestGame.Areas.Admin.Models
{
    public class SettingsSyncServiceViewModel
    {
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Дата, до которой следует очистить данные")]
        public DateTime Date { get; set; }
    }
}