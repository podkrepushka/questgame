﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DotNet.Highcharts;

namespace QuestGame.Areas.Admin.Models.Statistics
{
    public class ActiveOrPercentViewModel
    {
        [Required]
        [Display(Name = "Начало периода")]
        public DateTime? StartDate { get; set; }

        [Required]
        [Display(Name = "Конец периода")]
        public DateTime? EndDate { get; set; }

        [Required]
        [Display(Name = "Сценарий игры")]
        public int GameID { get; set; }

        [Display(Name = "Учебные заведения")]
        public List<int> SchoolsID { get; set; }

        public SelectList Schools { get; set; }

        public SelectList Games { get; set; }

        public Highcharts Chart { get; set; }
    
    }
}