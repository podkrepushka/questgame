﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuestGame.Areas.Admin.Models.Statistics
{
    public class ActiveUsersDate
    {
        public DateTime DateAnswer { get; set; }

        public int CountAnswers { get; set; }
    }
}