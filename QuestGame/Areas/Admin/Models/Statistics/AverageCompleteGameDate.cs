﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QuestGame.Areas.Admin.Models.Statistics
{
    public class AverageCompleteGameDate
    {
        public DateTime DateAnswer { get; set; }

        public double PercentCompleteGame { get; set; }
    }
}