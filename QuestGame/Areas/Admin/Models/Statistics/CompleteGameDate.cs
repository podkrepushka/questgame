﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuestGame.Areas.Admin.Models.Statistics
{
    public class CompleteGameDate
    {

        private int[,] Percents;
        public CompleteGameDate(int interval = 5)
        {
            Percents = new int[100/interval+1, 2];
            for (int i = 0; i < Percents.GetLength(0); i++)
            {
                Percents[i,0] = i * interval;
            }
        }

        public List<Tuple<int,int>> GetStatsCompleteGame()
        {
            var objectFormat = new List<Tuple<int, int>>();
            for (int i = 0; i < Percents.GetLength(0); i++)
            {
                objectFormat.Add(new Tuple<int, int>(Percents[i, 0], Percents[i, 1]));
            }
            return objectFormat;
        }

        public object[] HelpGraphGetValueXY()
        {
            var result = new List<object>();
            for (int i = 0; i < Percents.GetLength(0); i++)
            {
                result.Add(new { X = Percents[i, 0], Y = Percents[i, 1] });
            }
            return result.ToArray();
        }
        public void AddPercentCompleteGame(double percent)
        {
            if (percent.Equals(100))
            {
                Percents[Percents.GetLength(0) - 1, 1]++;
                return;
            }
            for (int i = 0; i < Percents.GetLength(0) - 1; i++)
            {
                if (percent >= Percents[i,0] & percent < Percents[i+1,0])
                {
                    Percents[i, 1]++;
                    break;
                }
            }
        }

    }
}