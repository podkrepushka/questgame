﻿using DotNet.Highcharts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuestGame.Areas.Admin.Models.Statistics
{
    public class CompleteGameViewModel
    {
        [Required]
        [Display(Name = "Сценарий игры")]
        public int GameID { get; set; }

        
        //[DataType(DataType.Date)]
        //[DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd.MM.yy}", ApplyFormatInEditMode = true)]
        //[DisplayFormat(DataFormatString = "{0:dd.mm.yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        [Display(Name = "Дата прохождения игры")]
        public DateTime? Date { get; set; }

        [Display(Name = "Учебные заведения")]
        public List<int> SchoolsID { get; set; }

        public SelectList Schools { get; set; }

        public SelectList Games { get; set; }

        public Highcharts Chart { get; set; }
    }
}