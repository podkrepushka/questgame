﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QuestGame.Areas.Admin.Models.Users
{
    public class DetailsUserViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Display(Name = "Фамилия")]
        public string Surname { get; set; }

        [Display(Name = "Учебное заведение")]
        public string School { get; set; }

        [Display(Name = "Преметы сдаваемые на ЕГЭ")]
        public string Subjects { get; set; }

        [Display(Name = "Роль")]
        public string Role { get; set; }

        [Display(Name = "Дата регистрации")]
        public DateTime CreateAccount { get; set; }
    }
}