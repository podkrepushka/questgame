﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuestGame.Areas.Admin.Models.Users
{
    public class ListUsersViewModel
    {
        public IEnumerable<IndexUserViewModel> Users { get; set; }
        public SelectList Schools { get; set; }
        public SelectList Subjects { get; set; }
    }
}