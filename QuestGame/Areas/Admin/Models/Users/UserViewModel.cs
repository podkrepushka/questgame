﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using QuestGame.Models;
using System.Web.Mvc;

namespace QuestGame.Areas.Admin.Models.Users
{
    public class UserViewModel
    {
        public UserViewModel()
        { }

        public int Id { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }

        //[Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Фамилия")]
        public string Surname { get; set; }

        
        [Display(Name = "Учебное заведение")]
        public int? SchoolID { get; set; }

        [Display(Name = "Преметы сдаваемые на ЕГЭ")]
        public List<int> SubjectsID { get; set; }

        [Required]
        [Display(Name = "Роль")]
        public string Role { get; set; }

        public SelectList Schools { get; set; }
        public SelectList Subjects { get; set; }
        public SelectList Roles { get; set; }

    }
}