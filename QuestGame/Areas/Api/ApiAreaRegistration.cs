﻿using System.Web.Mvc;

namespace QuestGame.Areas.Api
{
    public class ApiAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Api";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
           // context.MapRoute(
           //    "Api_sync",
           //    "Api/{controller}/{action}/{DateSync}",
           //    new { DateSync = UrlParameter.Optional }
           //);
           // context.MapRoute(
           //     "Api_default",
           //     "Api/{controller}/{id}",
           //     new { id = UrlParameter.Optional }
           // );
           
        }
    }
}