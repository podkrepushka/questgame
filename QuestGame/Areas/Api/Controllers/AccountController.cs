﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;
using QuestGame.Areas.Api.Models;
using QuestGame.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Data;
using System.Data.Entity;
using AutoMapper;

namespace QuestGame.Areas.Api.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private ApplicationUserManager _userManager;

        public AccountController()
        { }

        public AccountController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        ///POST: api/Account/Register
        ///
        [Route("Register")]
        [HttpPost]
        public async Task<IHttpActionResult> Register([FromBody] RegisterViewModel model)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            Mapper.CreateMap<RegisterViewModel, ApplicationUser>()
                      .ForMember(cu => cu.UserName, opt => opt.MapFrom(u => u.Email))
                      .ForMember(cu => cu.CreateAccount, opt => opt.MapFrom(u => DateTime.Now));

            var user = Mapper.Map<RegisterViewModel, ApplicationUser>(model);
            try
            {
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    using (var db = new ApplicationDbContext())
                    {
                        var usertmp = await db.Users.FirstAsync(u => u.Id.Equals(user.Id));
                        foreach (var subjectID in model.SubjectsID)
                        {
                            usertmp.Subjects.Add(await db.Subjects.FindAsync(subjectID));
                        }
                        db.Entry(usertmp).State = EntityState.Modified;
                        await db.SaveChangesAsync();
                    }
                    try
                    {
                        await UserManager.AddToRoleAsync(user.Id, "User");
                        string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        var callbackUrl = Url.Link("Default", new { Area = "", Controller = "Account", Action = "ConfirmEmail", userId = user.Id, code = code });
                        await UserManager.SendEmailAsync(user.Id, "Подтверждение учетной записи", "Подтвердите вашу учетную запись, щелкнув <a href=\"" + callbackUrl + "\">здесь</a>");
                        return Ok();
                    }
                    catch
                    {
                        return StatusCode((HttpStatusCode)513);
                    }
                }
                else
                {
                    return StatusCode((HttpStatusCode)512);
                }

            }
            catch
            {
                return StatusCode((HttpStatusCode)203);
            }
            

            
        }

        [Route("Login")]
        [HttpPost]
        public async Task<IHttpActionResult> Login(LoginViewModel model)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var userIdentity = UserManager.FindAsync(model.Email, model.Password).Result;
            if (userIdentity != null)
            {
                if (!await UserManager.IsEmailConfirmedAsync(userIdentity.Id))
                    return StatusCode((HttpStatusCode)515);
                else
                {
                    var identity = new ClaimsIdentity(Startup.OAuthOptions.AuthenticationType);
                    identity.AddClaim(new Claim(ClaimTypes.Name, model.Email));
                    identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, userIdentity.Id.ToString()));/////
                    AuthenticationTicket ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
                    var currentUtc = new SystemClock().UtcNow;
                    ticket.Properties.IssuedUtc = currentUtc;
                    ticket.Properties.ExpiresUtc = currentUtc.Add(TimeSpan.FromDays(1));
                    string AccessToken = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);

                    var loginPack = new LoginPack()
                    {
                        Token = AccessToken,
                        Name = userIdentity.Name,
                        Surname = userIdentity.Surname,
                        SchoolID = userIdentity.SchoolID
                    };

                    return Ok(loginPack);
                }
            }
            else
                return StatusCode((HttpStatusCode)514);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }
            }

            base.Dispose(disposing);
        }


    }
}
