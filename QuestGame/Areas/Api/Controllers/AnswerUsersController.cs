﻿using Microsoft.AspNet.Identity.Owin;
using QuestGame.Models;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
//using System.Web.Mvc;

namespace QuestGame.Areas.Api.Controllers
{
    [Authorize]
    public class AnswerUsersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private ApplicationUserManager _userManager;

        public AnswerUsersController()
        { }

        public AnswerUsersController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // POST: api/AnswerUsers
        [ResponseType(typeof(AnswerUser))]
        public async Task<IHttpActionResult> PostAnswerUser(AnswerUser answerUser)
        {
            if (ModelState.IsValid)
            {
                var stage = await db.Stages.FindAsync(answerUser.StageID);
                if (stage == null)
                {
                    return StatusCode(HttpStatusCode.NonAuthoritativeInformation);
                }
                answerUser.UserID = (await UserManager.FindByNameAsync(User.Identity.Name)).Id;
                if (stage.Answer.Equals(answerUser.Answer, System.StringComparison.OrdinalIgnoreCase)) // Сравнение на правильный ответ
                {
                    answerUser.AnswerCorrect = true;
                }
                else
                {
                    answerUser.AnswerCorrect = false;
                }
                db.AnswersUser.Add(answerUser);
                await db.SaveChangesAsync();
                if (answerUser.AnswerCorrect)
                {
                    return Ok();
                }
                else
                {
                    return StatusCode((HttpStatusCode)516);
                }
            }
            return BadRequest(ModelState);    
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}