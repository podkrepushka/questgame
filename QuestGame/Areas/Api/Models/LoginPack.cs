﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuestGame.Areas.Api.Models
{
    public class LoginPack
    {
        public string Token { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int? SchoolID { get; set; }
    }
}