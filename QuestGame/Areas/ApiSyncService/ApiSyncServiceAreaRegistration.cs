﻿using System.Web.Mvc;
using System.Web.Http;

namespace QuestGame.Areas.ApiSyncService
{
    public class ApiSyncServiceAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ApiSyncService";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.MapHttpRoute(
                name: "ApiSyncService_default",
                routeTemplate: "api/syncservice/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
             );
        }
    }
}