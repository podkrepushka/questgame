﻿using QuestGame.Areas.ApiSyncService.Models;
using QuestGame.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace QuestGame.Areas.ApiSyncService.Controllers
{
    [Authorize]
    public class GamesController : ApiController
    {

        private ApplicationDbContext db = new ApplicationDbContext();


        // GET: 
        public IQueryable<Game> GetGames()
        {
            return db.Games;
        }

        // GET:
        public async Task<IHttpActionResult> GetGamesUpdated(DateTime DateSync)
        {
            if (DateSync == null)
            {
                return BadRequest();
            }
            var games = db.Games.AsNoTracking()
                                .Where(g => g.DateSync > DateSync);

            var gamesDelete = db.GamesDelete.AsNoTracking()
                                            .Where(g => g.DateDelete > DateSync);

            var GamesPack = new SyncPack<Game, GameDelete>(await games.ToListAsync(),
                                                           await gamesDelete.ToListAsync());
            return Ok(GamesPack);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
