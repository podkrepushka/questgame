﻿using QuestGame.Areas.ApiSyncService.Models;
using QuestGame.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace QuestGame.Areas.ApiSyncService.Controllers
{
    [Authorize]
    public class LocationsController : ApiController
    {

        private ApplicationDbContext db = new ApplicationDbContext();


        // GET: 
        public IQueryable<Location> GetLocations()
        {
            return db.Locations;
        }

        // GET: 
        public async Task<IHttpActionResult> GetLocationsUpdated(DateTime DateSync)
        {
            if (DateSync == null)
            {
                return BadRequest();
            }
            var locations = db.Locations.AsNoTracking()
                                        .Where(l => l.DateSync > DateSync);

            var locationsDelete =  db.LocationsDelete.AsNoTracking()
                                                     .Where(l => l.DateDelete > DateSync);

            var LocPack = new SyncPack<Location, LocationDelete>(await locations.ToListAsync(),
                                                                 await locationsDelete.ToListAsync());
            return Ok(LocPack);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
