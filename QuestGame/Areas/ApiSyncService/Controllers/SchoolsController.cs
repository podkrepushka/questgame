﻿using QuestGame.Areas.ApiSyncService.Models;
using QuestGame.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace QuestGame.Areas.ApiSyncService.Controllers
{

    public class SchoolsController : ApiController
    {

        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: 
        public async Task<IHttpActionResult> GetSchools()
        {
            var schoolsPack = new List<SchoolPack>();
            foreach (var school in await db.Schools.AsNoTracking().Include(g => g.Games).ToListAsync())
            {
                schoolsPack.Add(new SchoolPack(school, (List<int>) school.Games.Select(i => i.ID).ToList()));
            }
            return Ok(schoolsPack);
        }

        /// GET: 
        public async Task<IHttpActionResult> GetSchoolsUpdated(DateTime DateSync)
        {
            if (DateSync == null)
            {
                return BadRequest();
            }
            var schoolsPack = new List<SchoolPack>();
            foreach (var school in await db.Schools.AsNoTracking().Where(l => l.DateSync > DateSync).Include(g => g.Games).ToListAsync())
            {
                schoolsPack.Add(new SchoolPack(school, school.Games.Select(i => i.ID).ToList()));
            }
            var schoolsDelete = db.SchoolsDelete.AsNoTracking().Where(l => l.DateDelete > DateSync); 
            var SchoolsPack = new SyncPack<SchoolPack, SchoolDelete>(schoolsPack, await schoolsDelete.ToListAsync());
            return Ok(SchoolsPack);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
