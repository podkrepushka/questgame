﻿using QuestGame.Areas.ApiSyncService.Models;
using QuestGame.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace QuestGame.Areas.ApiSyncService.Controllers
{
    [Authorize]
    public class StagesController : ApiController
    {

        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: 
        public IQueryable<Stage> GetStages()
        {
            return db.Stages;
        }

        /// GET: 
        public async Task<IHttpActionResult> GetStagesUpdated(DateTime DateSync)
        {
            if (DateSync == null)
            {
                return BadRequest();
            }
            var stages = db.Stages.AsNoTracking()
                                  .Where(s => s.DateSync > DateSync);
                                        
            var stagesDelete =  db.StagesDelete.AsNoTracking()
                                               .Where(s => s.DateDelete > DateSync);
                                       
            var StagesPack = new SyncPack<Stage, StageDelete>(await stages.ToListAsync(),
                                                              await stagesDelete.ToListAsync());
            return Ok(StagesPack);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
