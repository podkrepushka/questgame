﻿using QuestGame.Areas.ApiSyncService.Models;
using QuestGame.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace QuestGame.Areas.ApiSyncService.Controllers
{
    public class SubjectsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        // GET: 
        public IQueryable<Subject> GetSubjects()
        {
            return db.Subjects;
        }

        // GET: 
        public async Task<IHttpActionResult> GetSubjectsUpdated(DateTime DateSync)
        {
            if (DateSync == null)
            {
                return BadRequest();
            }
            var subjects = db.Subjects.AsNoTracking()
                                      .Where(s => s.DateSync > DateSync);

            var subjectsDelete = db.SubjectsDelete.AsNoTracking()
                                                  .Where(s => s.DateDelete > DateSync);

            var SubPack = new SyncPack<Subject, SubjectDelete>(await subjects.ToListAsync(),
                                                               await subjectsDelete.ToListAsync());
            return Ok(SubPack);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
