﻿using QuestGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuestGame.Areas.ApiSyncService.Models
{
    public class SchoolPack
    {
        public SchoolPack(School school,List<int> gamesID)
        {
            School = school;
            GamesID = gamesID;
        }
        public School School;

        public List<int> GamesID;
    }
}