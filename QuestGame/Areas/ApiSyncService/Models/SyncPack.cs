﻿using QuestGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;
using System.Threading.Tasks;

namespace QuestGame.Areas.ApiSyncService.Models
{
    public class SyncPack<T,T1>
    {
        public SyncPack(List<T> SyncData, List<T1> DeleteData)
        {
            this.SyncData = SyncData;
            this.DeleteData = DeleteData;
        }

        public List<T> SyncData;

        public List<T1> DeleteData;
    }
}