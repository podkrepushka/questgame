﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

using NPOI.HSSF.UserModel;
using NPOI.HSSF.Model;
using NPOI.SS.UserModel;

namespace QuestGame.Extensions
{
    public static class ExportExcelExtend
    {
        public static byte[] ExportExcel<T>(this IEnumerable<T> DataList, List<string> Titles, List<string[]> ExtraData = null, string NameSheet = "Report")
        {
            var workbook = new HSSFWorkbook();
            var worksheet = workbook.CreateSheet(NameSheet);

            int index = 0;
            if (ExtraData != null)
            {
                for (int i = 0; i < ExtraData.Count; i++)
                {
                    var tmpRow = worksheet.CreateRow(i);
                    for (int j = 0; j < ExtraData[i].Length; j++)
                    {
                        tmpRow.CreateCell(j).SetCellValue(ExtraData[i][j]);
                        worksheet.AutoSizeColumn(j);
                    }
                }
                index = ExtraData.Count + 1;
            }

            var row = worksheet.CreateRow(index);
            for (int i = 0; i < Titles.Count; i++)
            {
                row.CreateCell(i).SetCellValue(Titles[i]);
                worksheet.AutoSizeColumn(i);
            }

            for (int i = 1; i < DataList.Count() + 1; i++)
            {
                var tmpRow = worksheet.CreateRow(index + i);
                var valueList = DataList.ElementAt(i - 1).GetPropertyValues();
                for (int j = 0; j < valueList.Count; j++)
                {
                    var s = tmpRow.CreateCell(j);
                    tmpRow.CreateCell(j).SetCellValue(valueList[j]);
                    worksheet.AutoSizeColumn(j);
                }
            }
            var ms = new MemoryStream();
            workbook.Write(ms);
            return ms.ToArray();
        }

        private static List<string> GetPropertyValues<T>(this T data)
        {
            var propertyValues = new List<string>();
            var propertyInfos = data.GetType().GetProperties();
            foreach (var propertyInfo in propertyInfos)
            {
                if (propertyInfo.GetValue(data, null) is DateTime)
                {
                    propertyValues.Add((propertyInfo.GetValue(data, null) as DateTime?).Value.ToShortDateString());
                }
                else
                    propertyValues.Add(propertyInfo.GetValue(data, null).ToString());
            }
            return propertyValues;
        }

    }
}