﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;


namespace QuestGame.Helpers
{
    public static class ChartHelper
    {
        public static Highcharts GetChart(ChartTypes chartType, AxisTypes axisType, object[] xyValue, string xTitle, string yTitle, string title, string nameSeries, string subTitle = null)
        {
            Highcharts ResultChart = new DotNet.Highcharts.Highcharts("chart")
                               .InitChart(new Chart { DefaultSeriesType = chartType })
                               .SetTitle(new Title
                               {
                                   Text = title
                               })
                               .SetXAxis(GetXAxis(xTitle, axisType))
                               .SetYAxis(new YAxis
                               {
                                   Title = new YAxisTitle { Text = yTitle }
                               })
                               .SetTooltip(new Tooltip
                               {
                                   Enabled = true,
                                   PointFormat = yTitle + ": {point.y}"
                               })
                               .SetSeries(new[]
                               {
                                    new Series 
                                    {
                                        Name = nameSeries,
                                        Data = new Data(xyValue)
                                    }
                               });
            ResultChart.SetOptions(new GlobalOptions
                                    {
                                        Global = new Global { UseUTC = false },
                                        Lang = new DotNet.Highcharts.Helpers.Lang
                                        {
                                            Loading = "Загрузка...",
                                            Months = new string[] { "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" },
                                            Weekdays = new string[] { "Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота" },
                                            ShortMonths = new string[] { "Янв", "Фев", "Март", "Апр", "Май", "Июнь", "Июль", "Авг", "Сент", "Окт", "Нояб", "Дек" },
                                            ExportButtonTitle = "Экспорт",
                                            PrintButtonTitle = "Печать",
                                            DownloadPNG = "Скачать PNG",
                                            DownloadJPEG = "Скачать JPEG",
                                            DownloadPDF = "Скачать PDF",
                                            DownloadSVG = "Скачать SVG", 
                                        }
                                    });
            if (subTitle != null)
            {
                ResultChart.SetSubtitle(new Subtitle { Text = subTitle });
            }
            return ResultChart;

        }
        private static XAxis GetXAxis(string xTitle, AxisTypes axisType)
        {
            var xaxis = new XAxis();
            xaxis.Title = new XAxisTitle { Text = xTitle };
            xaxis.Type = axisType;
            if (axisType == AxisTypes.Datetime)
            {
                xaxis.DateTimeLabelFormats = new DateTimeLabel { Month = "%e. %b", Year = "%b" };
            }
            return xaxis;
        }
    }
}