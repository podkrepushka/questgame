﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace QuestGame.Helpers.Html
{
    public static class HtmlHelperDatePicker
    {
        public static MvcHtmlString BootstrapDatePicker<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, DateTime?>> fieldExpression)//string Name,DateTime? Date)
        {
            var fieldmetadata = ModelMetadata.FromLambdaExpression(fieldExpression, helper.ViewData);
            var fieldName = ExpressionHelper.GetExpressionText(fieldExpression);
            string value = ((DateTime?)fieldmetadata.Model).HasValue ? fieldmetadata.Model.ToString() : null;

            TagBuilder divGroup = new TagBuilder("div");
            divGroup.AddCssClass("input-group date");
            divGroup.GenerateId("Datepicker" + fieldName);
            TagBuilder input = new TagBuilder("input");
            input.AddCssClass("form-control");
            input.GenerateId(fieldName);
            input.Attributes.Add(new KeyValuePair<string, string>("name", fieldName));
            input.Attributes.Add(new KeyValuePair<string, string>("value", value));
            input.MergeAttributes(helper.GetUnobtrusiveValidationAttributes(fieldName));
            input.Attributes["type"] = "text";
            input.Attributes.Remove("data-val-date");
            divGroup.InnerHtml += input;
            TagBuilder span = new TagBuilder("span");
            span.AddCssClass("input-group-addon");
            TagBuilder subspan = new TagBuilder("span");
            subspan.AddCssClass("glyphicon glyphicon-calendar");
            span.InnerHtml += subspan;
            divGroup.InnerHtml += span;

            TagBuilder scripts = new TagBuilder("script");
            string datepicker = "$(function () { $('#Datepicker" + fieldName + "').datetimepicker({locale: 'ru',format: 'DD.MM.YYYY'});});";
            scripts.InnerHtml += datepicker;
            return new MvcHtmlString(divGroup.ToString() + scripts.ToString());
        }
    }
}