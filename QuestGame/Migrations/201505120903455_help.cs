namespace QuestGame.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class help : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Stages", "Help", c => c.String(maxLength: 3000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Stages", "Help", c => c.String(nullable: false, maxLength: 3000));
        }
    }
}
