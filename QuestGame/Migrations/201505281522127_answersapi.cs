namespace QuestGame.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class answersapi : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AnswerUsers", "AnswerCorrect", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AnswerUsers", "AnswerCorrect", c => c.Boolean());
        }
    }
}
