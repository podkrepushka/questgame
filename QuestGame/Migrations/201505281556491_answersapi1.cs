namespace QuestGame.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class answersapi1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AnswerUsers", "AnswerCorrect", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AnswerUsers", "AnswerCorrect", c => c.Boolean(nullable: false));
        }
    }
}
