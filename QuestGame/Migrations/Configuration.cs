namespace QuestGame.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using QuestGame.Models;
    internal sealed class Configuration : DbMigrationsConfiguration<QuestGame.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;// true; //
            ContextKey = "QuestGame.Models.ApplicationDbContext";
        }

        protected override void Seed(QuestGame.Models.ApplicationDbContext context)
        {

            //context.Location.Add(new Location() { Name = "1 ����" });

            //var game = new Game()
            //{
            //    Name = "game1",
            //    LocationID = 1,
            //    DateSync = DateTime.Now,
            //    Description = "asfsa"
            //};
            //context.Games.Add(game);

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
