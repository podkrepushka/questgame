namespace QuestGame.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    
    public partial class AnswerUser
    {
        public AnswerUser()
        {
            DateAnswer = DateTime.Now;
        }
        public int ID { get; set; }

        public int UserID { get; set; }

        [Required]
        public int StageID { get; set; }

        [Required]
        [StringLength(3000)]
        [Display(Name = "�����")]
        public string Answer { get; set; }

        [Display(Name = "���� ������")]
        public DateTime DateAnswer { get; set; }

        [Display(Name = "������������ ������")]
        public bool AnswerCorrect { get; set; }

        [Display(Name = "������������� ���������")]
        public bool? UseHelp { get; set; }

        public virtual Stage Stage { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}
