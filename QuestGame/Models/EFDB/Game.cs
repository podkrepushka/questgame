namespace QuestGame.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using Newtonsoft.Json;

    [JsonObject]
    public partial class Game
    {
        public Game()
        {
            Schools = new HashSet<School>();
            Stages = new HashSet<Stage>();
            DateSync = DateTime.Now;
            
        }

        public int ID { get; set; }

        
        [Display(Name = "����� ��������� �����")]
        public int LocationID { get; set; }
        
        [Required]
        [StringLength(50)]
        [Display(Name = "�������� ��������")]
        public string Name { get; set; }

        [StringLength(500)]
        [Display(Name = "��������")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "���� ���������� ����������")]
        public DateTime DateSync { get; set; }

        [JsonIgnore]
        public virtual ICollection<School> Schools { get; set; }
        
        [JsonIgnore]
        public virtual ICollection<Stage> Stages { get; set; }

        [JsonIgnore]
        public virtual Location Location { get; set; }


    }
}
