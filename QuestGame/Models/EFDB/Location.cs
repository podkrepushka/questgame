﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QuestGame.Models;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace QuestGame.Models
{
    [JsonObject]
    public partial class Location
    {
        public Location()
        {
            Stages = new HashSet<Stage>();
            DateSync = DateTime.Now;
        }


        public int ID { get; set; }

        [Required]
        [Display(Name = "Местоположение")]
        [StringLength(1000)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Дата последнего обновления")]
        public DateTime DateSync { get; set; }

        [JsonIgnore]
        public virtual ICollection<Stage> Stages { get; set; }

    }
}