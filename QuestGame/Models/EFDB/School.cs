namespace QuestGame.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using Newtonsoft.Json;

    [JsonObject]
    public partial class School
    {
        public School()
        {
            Users = new HashSet<ApplicationUser>();
            Games = new HashSet<Game>();
            DateSync = DateTime.Now;
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Название учебного заведения")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Дата последнего обновления")]
        public DateTime DateSync { get; set; }

        [JsonIgnore]
        public virtual ICollection<Game> Games { get; set; }
        [JsonIgnore]
        public virtual ICollection<ApplicationUser> Users { get; set; }

        public void Update()
        {
            this.DateSync = DateTime.Now;
        }
    }
}
