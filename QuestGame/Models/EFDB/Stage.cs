namespace QuestGame.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using Newtonsoft.Json;

    [JsonObject]
    public partial class Stage
    {
        public Stage()
        {
            AnswersUser = new HashSet<AnswerUser>();
            DateSync = DateTime.Now;
        }

        public int ID { get; set; }

        [Required]
        [Display(Name = "�������� ����")]
        public int GameID { get; set; }

       
        [Display(Name = "�������������� QR-����")]
        public int LocationID { get; set; }

        [Required]
        [StringLength(3000)]
        [Display(Name = "������")]
        public string Question { get; set; }

        [JsonIgnore] // ��� �������������, ����� �� ������������ �������
        [Required]
        [StringLength(3000)]
        [Display(Name = "�����")]
        public string Answer { get; set; }


        [StringLength(3000)]
        [Display(Name = "���������")]
        public string Help { get; set; }

        [Required]
        [StringLength(200)]
        [Display(Name = "QR-��� �������")]
        public string QRQuestion { get; set; }

        [Required]
        public DateTime DateSync { get; set; }

        //[Required]
        //[StringLength(200)]
        //[Display(Name = "QR-��� ������")]
        //public string QRAnswer { get; set; }

        [Required]
        [Display(Name = "����� �����")]
        public int Position { get; set; }

        [JsonIgnore]
        public virtual Game Game { get; set; }

        [JsonIgnore]
        public virtual Location Location { get; set; }

        [JsonIgnore]
        public virtual ICollection<AnswerUser> AnswersUser { get; set; }


    }
}
