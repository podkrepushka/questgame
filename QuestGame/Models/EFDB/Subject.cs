﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QuestGame.Models;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace QuestGame.Models
{
    [JsonObject]
    public partial class Subject
    {
        public Subject()
        {
            Users = new HashSet<ApplicationUser>();
            DateSync = DateTime.Now;
        }


        public int ID { get; set; }

        [Required]
        [Display(Name = "Предмет")]
        [StringLength(1000)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Дата последнего обновления")]
        public DateTime DateSync { get; set; }

        [JsonIgnore]
        public virtual ICollection<ApplicationUser> Users { get; set; }

    }

}