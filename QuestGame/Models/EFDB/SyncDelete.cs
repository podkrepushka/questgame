﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuestGame.Models
{
    public class SyncDelete
    {
        public SyncDelete()
        {
            DateDelete = DateTime.Now;
        }

        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }
        [Required]
        public DateTime DateDelete { get; set; }
    }
}