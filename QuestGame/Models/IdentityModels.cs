﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System;

namespace QuestGame.Models
{
    
    // Чтобы добавить данные профиля для пользователя, можно добавить дополнительные свойства в класс ApplicationUser. Дополнительные сведения см. по адресу: http://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser<int, CustomUserLogin, CustomUserRole,CustomUserClaim> 
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, int> manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Здесь добавьте утверждения пользователя
            return userIdentity;
        }

        public ApplicationUser()
        {
            AnswersUser = new HashSet<AnswerUser>();
            Subjects = new HashSet<Subject>();
        }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime CreateAccount { get; set; }
        public int? SchoolID { get; set; }
        public virtual School School { get; set; }
        public virtual ICollection<AnswerUser> AnswersUser { get; set; }
        public virtual ICollection<Subject> Subjects { get; set; }
    }


    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, CustomRole,
    int, CustomUserLogin, CustomUserRole, CustomUserClaim>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<School> Schools { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Stage> Stages { get; set; }
        public DbSet<AnswerUser> AnswersUser { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<LocationDelete> LocationsDelete { get; set; }
        public DbSet<SchoolDelete> SchoolsDelete { get; set; }
        public DbSet<GameDelete> GamesDelete { get; set; }
        public DbSet<StageDelete> StagesDelete { get; set; }
        public DbSet<SubjectDelete> SubjectsDelete { get; set; }
       

    }
    //Установка для первичного ключа тип int
    public class CustomUserRole : IdentityUserRole<int> { }
    public class CustomUserClaim : IdentityUserClaim<int> { }
    public class CustomUserLogin : IdentityUserLogin<int> { }

    public class CustomRole : IdentityRole<int, CustomUserRole>
    {
        public CustomRole() { }
        public CustomRole(string name) { Name = name; }
    }

    public class CustomUserStore : UserStore<ApplicationUser, CustomRole, int,
        CustomUserLogin, CustomUserRole, CustomUserClaim>
    {
        public CustomUserStore(ApplicationDbContext context)
            : base(context)
        {
        }
    }

    public class CustomRoleStore : RoleStore<CustomRole, int, CustomUserRole>
    {
        public CustomRoleStore(ApplicationDbContext context)
            : base(context)
        {
        }
    } 
}