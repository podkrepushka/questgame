﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QuestGame.Startup))]
namespace QuestGame
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
