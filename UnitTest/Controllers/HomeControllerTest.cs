﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QuestGame.Controllers;
using System.Web.Mvc;


namespace UnitTest.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void IndexViewResultNotNull()
        {
            HomeController homeController = new HomeController();
            ViewResult result = homeController.Index() as ViewResult;
            Assert.IsNotNull(result);
        }
    }
}
